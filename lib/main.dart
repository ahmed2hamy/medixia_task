import 'package:flutter/material.dart';
import 'package:medixia_task/model/appointment_model.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  String _title = 'Medixia Task';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: _title),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final ScrollController _scrollController = ScrollController();

  AppointmentsModel _appointmentsModel = AppointmentsModel(
    appointmentDays: [
      AppointmentDay(day: "SAT", appointmentDays: [
        AppointmentTime(timeFrom: "04:00", timeTo: "06:00")
      ]),
      AppointmentDay(day: "SUN", appointmentDays: [
        AppointmentTime(timeFrom: "04:00", timeTo: "06:00")
      ]),
      AppointmentDay(day: "MON", appointmentDays: [
        AppointmentTime(timeFrom: "04:00", timeTo: "06:00"),
        AppointmentTime(timeFrom: "04:00", timeTo: "06:00"),
      ]),
      AppointmentDay(day: "TUS", appointmentDays: [
        AppointmentTime(timeFrom: "04:00", timeTo: "06:00"),
        AppointmentTime(timeFrom: "04:00", timeTo: "06:00"),
      ]),
    ],
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueAccent,
      body: Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          height: MediaQuery.of(context).size.height * 0.8,
          padding: const EdgeInsets.only(top: 30),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(24),
          ),
          child: SingleChildScrollView(
            controller: _scrollController,
            child: Column(
              children: [
                _screenHeader(),
                _scheduleListView(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _screenHeader() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              CircleAvatar(
                backgroundImage: AssetImage("assets/doctor.png"),
                radius: 30.0,
              ),
              SizedBox(width: 10),
              Text("Hello Dr Lama"),
            ],
          ),
          SizedBox(height: 15),
          Text("Kindly check your availability and set your schedule"),
          SizedBox(height: 15),
          Center(
            child: RaisedButton(
              onPressed: () {},
              child: Text(
                "Add new time slot",
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              shape: StadiumBorder(),
              color: Colors.pinkAccent,
            ),
          ),
          SizedBox(height: 15),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("Available working hours"),
              Text(
                "Days: " + _appointmentsModel.appointmentDays.length.toString(),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _scheduleListView() {
    return ListView.builder(
      controller: _scrollController,
      shrinkWrap: true,
      itemCount: _appointmentsModel.appointmentDays.length,
      itemBuilder: (context, index) {
        return _itemBuilder(index);
      },
    );
  }

  Widget _itemBuilder(int index) {
    return Container(
      color: index.isEven ? Colors.grey[200] : Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
              child: Center(
                  child: Text(_appointmentsModel.appointmentDays[index].day))),
          _separatorWidget(),
          Expanded(
            flex: 4,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: _appointmentsModel
                    .appointmentDays[index].appointmentDays.length,
                itemBuilder: (context, ind) => _timesBuilder(_appointmentsModel
                    .appointmentDays[index].appointmentDays[ind]),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _timesBuilder(AppointmentTime time) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Column(
          children: [
            Text("From"),
            Text(time.timeFrom),
          ],
        ),
        SizedBox(
          width: 1,
        ),
        Column(
          children: [
            Text("To"),
            Text(time.timeTo),
          ],
        ),
        IconButton(
          icon: Icon(
            Icons.delete_forever,
            color: Colors.pinkAccent,
          ),
          onPressed: () {},
        ),
      ],
    );
  }

  _separatorWidget() {
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          width: 8,
          height: 8,
          decoration: BoxDecoration(
            color: Colors.pinkAccent,
            shape: BoxShape.circle,
          ),
        ),
        Container(
          width: 1,
          // height: 100,
          color: Colors.pinkAccent,
        ),
      ],
    );
  }
}
