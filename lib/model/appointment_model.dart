class AppointmentsModel {
  final List<AppointmentDay> appointmentDays;

  AppointmentsModel({this.appointmentDays});
}

class AppointmentDay {
  final String day;
  final List<AppointmentTime> appointmentDays;

  AppointmentDay({this.day, this.appointmentDays});
}

class AppointmentTime {
  final String timeFrom;
  final String timeTo;

  AppointmentTime({this.timeFrom, this.timeTo});
}
